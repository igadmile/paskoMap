// Variables
var map = L.map('map', {
    renderer: L.canvas(),
  }),
  basemapSatellite = L.tileLayer(
    'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
    {
      attribution:
        'Kartu izradila <a href="http://www.biom.hr/">Udruga BIOM</a>. Korištene podloge - <a href="https://www.esri.com/en-us/home">ESRI</a>',
    }
  ),
  basemapOutdoors = L.tileLayer(
    'https://{s}.tile.thunderforest.com/landscape/{z}/{x}/{y}.png?apikey=f13946a5090e4a73aded2506761ae4eb',
    {
      attribution:
        'Kartu izradila <a href="http://www.biom.hr/">Udruga BIOM</a>. Korištene podloge - <a href="https://www.thunderforest.com">Thunderforest</a>',
    }
  ),
  basemapDark = L.tileLayer(
    'https://cartodb-basemaps-c.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png',
    {
      attribution:
        'Kartu izradila <a href="http://www.biom.hr/">Udruga BIOM</a>. Korištene podloge - <a href="https://carto.com/">Carto</a>',
    }
  ).addTo(map),
  baseLayers = {
    'Carto-Dark': basemapDark,
    'Thunderforest-Outdoors': basemapOutdoors,
    'ESRI-Satellite': basemapSatellite,
  },
  selected = null,
  urlServer = 'http://localhost:8000/';

//Controlls
//L.control.scale().addTo(map);
L.control
  .layers(baseLayers, null, {
    collapsed: false,
  })
  .addTo(map);

// add slider to the control
L.Control.TimeSlider = L.Control.extend({
  onAdd: function() {
    var slider = L.DomUtil.create('div', 'slider');
    L.DomEvent.disableClickPropagation(slider);
    return slider;
  },
});

L.control.timeSlider = function(opts) {
  return new L.Control.TimeSlider(opts);
};
L.control
  .timeSlider({
    position: 'bottomleft',
  })
  .addTo(map);

//Funvtions
var select = function(layer) {
    selected = layer;
  },
  magnify = function(layer) {
    layer.setStyle({
      radius: 8.0,
      opacity: 1,
      color: 'rgba(255,255,255,1.0)',
      weight: 1.5,
      fillOpacity: 1,
      fillColor: 'rgba(255,186,39,1.0)',
    });
  },
  higlight = function(layer) {
    layer.setStyle({
      opacity: 1,
      color: 'rgba(55, 73, 106, 1)',
      weight: 5.0,
      fillOpacity: 0,
    });
  },
  resetStyle = function(layer, feature) {
    if (selected === null || selected._leaflet_id !== feature._leaflet_id) {
      layer === 'points'
        ? points.resetStyle(feature)
        : line.resetStyle(feature);
    }
  },
  onEachFeaturePoints = function(feature, layer) {
    var extractRegex = function(string) {
      var rx = /\ (\d{4})-(\d{2})-(\d{2})\ (\d{2}):(\d{2})/g,
        arr = rx.exec(string),
        hour = parseInt(arr[4]) + 2;
      return arr[3] + '.' + arr[2] + '.' + arr[1] + ' ' + hour + ':' + arr[5];
    };
    layer.on({
      mouseover: function(e) {
        magnify(e.target);
      },
      click: function(e) {
        select(e.target);
        magnify(e.target);
      },
      mouseout: function(e) {
        resetStyle('points', e.target);
      },
      popupclose: function(e) {
        select(null);
        resetStyle('points', e.target);
      },
    });
    var popupContent =
      "<div style='text-align: center; margin-left: auto; margin-right: auto;'><strong>" +
      extractRegex(feature.properties.Name) +
      '</strong></div>';
    layer.bindPopup(popupContent);
  },
  onEachFeatureLastPoint = function(feature, layer) {
    var extractRegex = function(string) {
      var rx = /\ (\d{4})-(\d{2})-(\d{2})\ (\d{2}):(\d{2})/g,
        arr = rx.exec(string),
        hour = parseInt(arr[4]) + 2;
      return arr[3] + '.' + arr[2] + '.' + arr[1] + ' ' + hour + ':' + arr[5];
    };
    var popupContent =
      "<div style='text-align: center; margin-left: auto; margin-right: auto;'><strong>" +
      extractRegex(feature.properties.Name) +
      '</strong></div>';
    layer.bindPopup(popupContent);
  },
  onEachFeatureLine = function(feature, layer) {
    layer.on({
      mouseover: function(e) {
        higlight(e.target);
      },
      click: function(e) {
        select(e.target);
        higlight(e.target);
      },
      mouseout: function(e) {
        resetStyle('line', e.target);
      },
      popupclose: function(e) {
        select(null);
        resetStyle('line', e.target);
      },
    });
    const distance = layer._latlngs.reduce(
      (distance, latnlngs, index, array) => {
        const i = index === 0 ? 1 : index;
        return distance + array[i - 1].distanceTo(latnlngs);
      },
      0
    );

    var popupContent =
      "<div style='text-align: center; margin-left: auto; margin-right: auto;'><strong>Paško je do sada preletio:</strong></div>" +
      "<div style='text-align: center; margin-left: auto; margin-right: auto;'>" +
      (distance / 1000).toFixed(2) +
      ' kilometara!</div>';
    layer.bindPopup(popupContent);
  };

//Layers
var line = null,
  points = null,
  lastPoint = null,
  pointsData = null;

var drawPoints = function() {
    points = new L.geoJson(null, {
      onEachFeature: onEachFeaturePoints,
      style: {
        radius: 4.5,
        opacity: 1,
        color: 'rgba(255,255,255,1.0)',
        weight: 1.5,
        fillOpacity: 1,
        fillColor: 'rgba(255,186,39,1.0)',
      },
      pointToLayer: function(feature, latlng) {
        return L.circleMarker(latlng);
      },
    });
  },
  genLineData = function(data) {
    var features = data,
      feature = {},
      c = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'LineString',
              coordinates: [],
            },
          },
        ],
      };

    Object.keys(features).forEach(function(key) {
      feature = features[key];
      c.features[0].geometry.coordinates.push(feature.geometry.coordinates);
    });
    return c;
  },
  drawLine = function() {
    line = new L.geoJson(null, {
      onEachFeature: onEachFeatureLine,
      style: {
        opacity: 1,
        color: 'rgba(46, 99, 93, 1)',
        weight: 3.0,
        fillOpacity: 0,
      },
    });
  },
  drawLastPoint = function() {
    lastPoint = new L.geoJson(null, {
      onEachFeature: onEachFeatureLastPoint,
    });
  };

var filterData = function(data, minDate, maxDate) {
  return data.features.filter((feature) => {
    const featureDate = new Date(feature.properties.Name.slice(19, 35));
    return (
      feature &&
      featureDate <= maxDate &&
      featureDate >= minDate &&
      feature.properties.Name !== 'START' &&
      feature.properties.Name !== 'END'
    );
  });
};

$.ajax({
  dataType: 'json',
  url: urlServer + 'data/jsonoutput/data.json',
  success: function(data) {
    pointsData = data;
    var dataFiltered = filterData(
      pointsData,
      new Date('2017-09-18 8:00'),
      new Date()
    );
    drawLine(dataFiltered);
    drawPoints(dataFiltered);
    drawLastPoint(dataFiltered);
    points.addData(dataFiltered);
    line.addData(genLineData(dataFiltered));
    lastPoint.addData(dataFiltered[dataFiltered.length - 1]);
    map.addLayer(line);
    map.addLayer(points);
    map.addLayer(lastPoint);

    map.fitBounds(line.getBounds());
  },
});

$('.slider').dateRangeSlider({
  bounds: {
    min: new Date('2017-09-18 8:00'),
    max: new Date(),
  },
  defaultValues: {
    min: new Date('2017-09-18 8:00'),
    max: new Date(),
  },
  arrows: false,
  formatter: function(val) {
    var days = val.getDate(),
      month = val.getMonth() + 1,
      year = val.getFullYear(),
      hours = val.getHours() + 2,
      minutes = val.getMinutes();
    if (hours > 24) {
      hours = 24;
    }
    return days + '.' + month + '.' + year + ' ' + hours + ':' + minutes;
  },
});

$('.slider').bind('valuesChanged valuesChanging', function(e, data) {
  //    remove existing layer and control
  if (map.hasLayer(points)) {
    points.clearLayers();
    line.clearLayers();
    lastPoint.clearLayers();
  }
  var dataFiltered = filterData(pointsData, data.values.min, data.values.max);
  line.addData(genLineData(dataFiltered));
  points.addData(dataFiltered);
  lastPoint.addData(dataFiltered[dataFiltered.length - 1]);
});

$(window).resize(function() {
  $('.slider').width(Math.round($(window).width() / 2.5));
  $('.ui-rangeSlider-container').width(Math.round($(window).width() / 2.5));
  $('.ui-rangeSlider-bar').width(Math.round($(window).width() / 2.5));
  $('.ui-rangeSlider-innerBar').width(Math.round($(window).width() / 2.5));
});
$(document).ready(function() {
  $('.slider').width(Math.round($(window).width() / 2.5));
  $('.ui-rangeSlider-container').width(Math.round($(window).width() / 2.5));
});

var playTrail = function(dateTime, sliderBounds, sliderSpeed) {
  var timeDiff = sliderBounds.max - sliderBounds.min;

  function loop() {
    $('#stop').click(function() {
      timeDiff = 0;
      $('#stop').css('display', 'none');
      $('#play').css('display', 'block');
      $('.slider').dateRangeSlider(
        'values',
        sliderBounds.min,
        sliderBounds.max
      );
    });
    setTimeout(function() {
      if (timeDiff > 0) {
        dateTime.setHours(dateTime.getHours() + 2);
        $('.slider').dateRangeSlider('values', sliderBounds.min, dateTime);
        timeDiff -= 7200000;
        loop();
      } else {
        $('#stop').css('display', 'none');
        $('#play').css('display', 'block');
      }
    }, sliderSpeed);
  }
  loop();
};

var start = $(
    '<div id="play" class="button-slider-play play-control"><img src="css/play-button.svg" style="width;auto;height:18px;"/></div>'
  ),
  stop = $(
    '<div id="stop" class="button-slider-play play-control"><img src="css/001-stop.svg" style="width;auto;height:18px;"/></div>'
  ),
  slowSpeed = $(
    '<div id="slow" class="button-slider-speed slow"><img src="css/003-snail-facing-right.svg" style="width;auto;height:18px;"/></div>'
  ),
  normalSpeed = $(
    '<div id="normal" class="button-slider-speed normal"><img src="css/002-coyote-facing-right.svg" style="width;auto;height:18px;"/></div>'
  ),
  fastSpeed = $(
    '<div id="fast" class="button-slider-speed fast"><img src="css/001-eagle.svg" style="width;auto;height:18px;"/></div>'
  );

$('.slider').append(start);
$('.slider').append(stop);
$('.slider').append(slowSpeed);
$('.slider').append(normalSpeed);
$('.slider').append(fastSpeed);
$('#stop').css('display', 'none');
$('#normal').addClass('button-slider-speed-selected');

var sliderSpeed = 80;
$('#slow').click(function() {
  sliderSpeed = 160;
  $('#slow').addClass('button-slider-speed-selected');
  $('#normal').removeClass('button-slider-speed-selected');
  $('#fast').removeClass('button-slider-speed-selected');
});
$('#normal').click(function() {
  sliderSpeed = 80;
  $('#normal').addClass('button-slider-speed-selected');
  $('#fast').removeClass('button-slider-speed-selected');
  $('#slow').removeClass('button-slider-speed-selected');
});
$('#fast').click(function() {
  sliderSpeed = 40;
  $('#fast').addClass('button-slider-speed-selected');
  $('#normal').removeClass('button-slider-speed-selected');
  $('#slow').removeClass('button-slider-speed-selected');
});
$('#play').click(function() {
  var dateTime = $('.slider').dateRangeSlider('values').min;
  playTrail(dateTime, $('.slider').dateRangeSlider('values'), sliderSpeed);
  $('#play').css('display', 'none');
  $('#stop').css('display', 'block');
});
