Interactive map for tracking the real time movement for Short-toed snake eagle named [Paško](http://pasko.biom.hr/).

Paško was tagged with the GPS GSM tracker which sends the location data via SMS to the GPS manufacturer system.

Location data is scraped from the GPS manufacturer system, converted to GeoJSON format and sent to the server using the python and beautiful soup package. The scraping script is not part of this repository.

The data is loaded via the ajax request.

# Technologies
- leafletjs
- jQuery
- jQAllRangeSliders

# Feature overview
![overview](https://gitlab.com/igadmile/paskoMap/raw/b0478414fba6be1fb9c5c0aa2cec7433185ccf06/overview.jpg)

Clicking on the movement track (green) opens the popup with numbe of cilometers that Paško flew during the selected interval.

Clicking on the yellow way points opens the popup displaying the date/time when the point was recorded.

Blue marker marks the last way point.

## Date control
Moving the control handels filters the data based on the selected date range and displays the data on the map.

## Animation control
- Clicking on the one of three animals in the animation control defines the animation speed (snail: slow, dog: normal, eagle: fast)
- Clicking on the "play" button plays the animation for the selected date interval

